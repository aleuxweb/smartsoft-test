var express = require('express'),
app = express(),
port = process.env.PORT || 3001,
mongoose = require('mongoose'),
Condition = require('./api/models/conditionsModel'),
bodyParser = require('body-parser');
  
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/ConditionsDB'); 

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./api/routes/conditionsRoutes');
routes(app);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);