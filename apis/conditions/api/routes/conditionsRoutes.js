'use strict';
module.exports = function(app) {
  var conditions = require('../controllers/conditionsController');

  app.route('/conditions')
    .get(conditions.list_all_conditions)
    .post(conditions.create_a_condition);


  app.route('/conditions/:conditionId')
    .get(conditions.read_a_condition)
    .put(conditions.update_a_condition)
    .delete(conditions.delete_a_condition);
};