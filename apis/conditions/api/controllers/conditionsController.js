'use strict';

var mongoose = require('mongoose'),
Condition = mongoose.model('Conditions');

exports.list_all_conditions = function(req, res) {
  Condition.find({}, function(err, condition) {
    if (err)
      res.send(err);
    res.json(condition);
  });
};

exports.create_a_condition = function(req, res) {
  var new_condition = new Condition(req.body);
  new_condition.save(function(err, condition) {
    if (err)
      res.send(err);
    res.json(condition);
  });
};

exports.read_a_condition = function(req, res) {
  Condition.findById(req.params.conditionId, function(err, condition) {
    if (err)
      res.send(err);
    res.json(condition);
  });
};

exports.update_a_condition = function(req, res) {
  Condition.findOneAndUpdate({_id: req.params.conditionId}, req.body, {new: true}, function(err, condition) {
    if (err)
      res.send(err);
    res.json(condition);
  });
};

exports.delete_a_condition = function(req, res) {
  Condition.remove({
    _id: req.params.conditionId
  }, function(err, condition) {
    if (err)
      res.send(err);
    res.json({ message: 'Condition successfully deleted' });
  });
};
