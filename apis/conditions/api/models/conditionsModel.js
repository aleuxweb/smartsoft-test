'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ConditionSchema = new Schema({
  name: {
    type: String,
    Required: 'Enter the name of the condition'
  },
  content:{
	  type: String,
	  Required: 'Enter the condition content'
  },
  Created_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Conditions', ConditionSchema);